# NPR Headline Grabber

Grabs a specified number of headlines fron the NPR text-only website and prints
them out. Updates if the cache is older than 1 hour and you are online.

## Getting started

### Prerequisites

- Python 3
- Internet connectivity

### Installing

- Clone this repo

### Running

- python3 npr.py [number of articles to display]

## License

This project is licensed under the GPLv3 - see the [LICENSE.md](LICENSE.md) file for details.