#!/usr/bin/env/python3
import json
import os
import random
import re
import requests
import sys
import time

URL = "https://text.npr.org/t.php?tid=1001"
LINK_PREFIX = "https://text.npr.org"
SAVED_HEADLINES = os.path.expanduser('~/.pynpr.json')
SEPARATOR = '\n  '  # separator between each article and its link
UPDATE_INTERVAL = 3600  # seconds between updates; default 3600 (1 hr)


def main(num_articles, download_articles, random_articles):
    if not download_articles:
        articles = load_cached_data()
    else:
        #print('Not using cached data')
        print('Fetching new data')
        articles = download_page()
        if articles is not None:#'articles' in locals():
            articles = [('Updated', current_time)] + articles
            with open(SAVED_HEADLINES, 'w') as f:
                json.dump(articles, f)
        else:
            articles = load_cached_data()

    # Pick articles to display
    # File structure:
    # 0 - time updated
    # 1 - date
    # 2-16 - articles
    display_articles = []
    if random_articles:
        display_articles = random.sample(range(2, 17), num_articles)
    else:
        display_articles = list(range(num_articles))
    for i in display_articles:
        print("{}{}{}".format(articles[i][0], SEPARATOR, articles[i][1]))


def load_cached_data():
    #print('Using cached data')
    with open(SAVED_HEADLINES, 'r') as f:
        articles = json.load(f)
    return articles


def download_page():
    try:
        page = requests.get(URL)
    except requests.exceptions.ProxyError:
        articles = load_cached_data()
        print('Unable to reach proxy; using cached data')
        return None
    except Exception as e:
        print('Error fetching articles; using cached data: {}'.format(e.__name__))
        return None
    articles = []
    if page.status_code == 200:
        page_html = page.text
        articles += parse_date(page_html)
        articles += parse_articles(page_html)
    else:
        articles += [('Error retreiving URL, returned', str(page.status_code))]
    return articles


def parse_date(page_html):
    # Match date inside a line of the format:
    # <p><a href="/">Home</a> &gt; Topic: News</p><p>May 31, 2018</p><ul>
    date_regex = r'<a href=\"\/\">Home<\/a>.+<p>(\w+\s\w+,\s\w{4})<\/p><ul>'
    date = [("NPR's Top Stories", re.search(date_regex, page_html).groups()[0])]
    return date


def parse_articles(page_html):
    # Match article titles and their links inside lines with the format:
    # <li><a href="/s.php?sId=615517016&rid=1001">Would The U.S. Withdraw Troops From South Korea?</a></li>
    article_regex = r'<li><a href=\"(\/\w\W\w{3}\W\w{3}\W\d+&\w{3}=\d{4})\">(.+?)<\/a><\/li>'
    article_data = re.findall(article_regex, page_html)
    # article_data is [(link stub, title)]
    # Need to change to [(title, full link)]
    articles = []
    for article in article_data:
        link = LINK_PREFIX + article[0]
        new_article = (article[1].strip(), link)
        articles += [new_article]
    return articles


if __name__ == '__main__':
    num_articles = 1  # NPR displays 15 articles; must be <=15 if using
    # random_articles = True or random.sample will throw an exception
    user_args = sys.argv[1:]
    if len(user_args) == 1:
        if user_args[0].isdigit():
            num_articles = int(user_args[0])
        elif user_args[0] in ['-h', '--help']:
            print('Usage:\n\tpython3 npr.py [n]\n\n\tn - the number of articles'
                  ' to use (range [0, 15]); overrides what is set in the file (optional)')
            sys.exit(0)
        else:
            print('Invalid argument(s). Use the "-h" flag for help.')
            sys.exit(1)
    random_articles = True  # pick article(s) to display randomly
    current_time = int(time.time())  # epoch time, in seconds
    download = True
    if os.path.exists(SAVED_HEADLINES):
        article_data = load_cached_data()
        if current_time - article_data[0][1] < UPDATE_INTERVAL:
            # Less than 1 hr has passed since last update
            download = False
    main(num_articles, download, random_articles)
    sys.exit(0)